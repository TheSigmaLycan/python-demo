# copy_demo.py
# Author: Rudhraa

from copy import deepcopy

my_list = [1, 2, 3, 4]

a = my_list

b = [1, 2, 3, 4]

c = a.copy()

d = deepcopy(a)
d[0] = 5

print(a, id(a))
print(b, id(b))
print(c, id(c))
print(d, id(d))
print(id(a[0]))
print(id(d[0]))
