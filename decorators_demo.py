# decorators_demo.py
# Author: Rudhraa

# Decorators are functions which takes another function as
# a parameter and adds some functionality to it and then
# returns the function

from time import time


def measure_time(func):
    def wrapper(*args):
        t = time()
        result = func(*args)
        print(f"Function took {time() - t} seconds")
        return result
    return wrapper


def detail(func):
    def inner(a, b):
        print(str(a), "+", str(b), "=", end=" ")
        return func(a, b)
    return inner


@detail
@measure_time
def sum_ab(a, b):
    summed = a + b
    print(summed)
    return summed


if __name__ == "__main__":
    x = sum_ab(1, 2)
    print(x)
