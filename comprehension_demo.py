# comprehension_demo.py
# Author: Rudhraa

my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

b = []

for x in my_list:
    b.append(x * x)

print(b)

# all the above lines of code is equivalent to
# the single line below

c = [x * x for x in range(1, 11)]
print(c)

# Similar method for dictionary comprehension
sample_dict = {k: k * k for k in my_list}
