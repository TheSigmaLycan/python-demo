# other_python_file.py
# Author: Rudhraa

__doc__ = """This is an example module"""


class TheOtherClass:
    """
    A Class just for demonstration purposes
    and this is its document string
    this is how doc_strings work
    """

    a_variable = "Hi"

    def __init__(self, *args) -> None:
        # print(self.a_variable)
        self.args = args

    def average(self) -> int:
        """
        A function to get the sum of a few numbers

        Returns:
            int: sum of all the inputs
        """
        return sum(self.args) / len(self.args)


if __name__ == "__main__":
    pass
