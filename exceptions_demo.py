import traceback


class DrinkingExceptions(Exception):
    def __init__(self, *args: object, **kwargs) -> None:
        super().__init__(*args)
        self.whatever_the_age_is = kwargs.get("age")


def custom_exception():
    try:
        age = int(input("Hey what's your age? "))
        if age < 18:
            raise DrinkingExceptions("AgeError: Underage", age=age)
        else:
            print("Go drink whatever you like...")
    except DrinkingExceptions as de:
        print(de, f"The user's age is {de.whatever_the_age_is}")


def std_exception():
    try:
        while True:
            expression = input("Enter expression: ")
            if expression == "":
                continue
            else:
                print(eval(expression))
    except Exception:

        """ get whatever the exception in string format """
        my_exception = traceback.format_exc()

        with open("error_log.txt", "a") as err_log:

            """ log the error in a text file"""
            err_log.write(my_exception + "\n")
    except KeyboardInterrupt:
        """
        Adding KeyBoardInterrupt is a good practice 
        it helps you to have control over the process
        """
        print("Terminated by user.")
    finally:
        print("Finally block")

# std_exception()
# custom_exception()
