# functions_demo.py
# Author: Rudhraa

# Declaring functions
def func1():
    print("my_func is called")


def func2(a, b) -> int:
    return sum(a, b)


def func3(a=None, b=None):
    pass


def func4(a, b=None):
    pass


def func5(*args):
    print(args)


def func6(**kwargs):
    print(kwargs)


# Anonymous or lambda functions
func7 = lambda n: int(n * (n + 1) / 2)


# Assigning functions to variables
func8: function = print("Hello")

# Calling the functions
func1()
func2(1, 2)
func3(1, 2)
func4(1, 2)
func5(1, 2)
func6(a=1, b=2)
func7(10)
func8()
