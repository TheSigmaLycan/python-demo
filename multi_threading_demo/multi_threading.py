import logging
from concurrent.futures import ThreadPoolExecutor
from os import cpu_count, getpid
from threading import Thread
from time import sleep

log_format = "%(asctime)s >>> %(message)s"
logging.basicConfig(format=log_format, level=logging.INFO, datefmt="%H:%M:%S")


def very_useful_func(n: int) -> None:
    # There's nothing else to do while this function is being executed
    # This function can run independent of the state of interpreter
    sleep(n)
    print(n)


if __name__ == "__main__":
    logging.info("Starting the main thread")

    useful_data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # threads = [Thread(target=very_useful_func, args=(x,)) for x in useful_data]
    # logging.info("Creating threads")

    # for t in threads:
    #     t.start()
    # logging.info("Starting sub-threads")

    # print([t.ident for t in threads])

    # for t in threads:
    #     t.join()
    # logging.info("Sub-threads ended")

    with ThreadPoolExecutor(10) as executor:
        executor.map(very_useful_func, useful_data)
