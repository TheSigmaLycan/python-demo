# class_demo.py
# Author: Rudhraa


class One:
    def __init__(self) -> None:
        super(One, self).__init__()
        print("One")


class Two:
    def __init__(self) -> None:
        super(Two, self).__init__()
        print("Two")


class Three(Two, One):
    def __init__(self) -> None:
        super(Three, self).__init__()
        print("Three")


obj1 = Three()
