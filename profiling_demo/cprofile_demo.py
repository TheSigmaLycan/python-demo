# cprofile_demo.py
# Author: Rudhraa


def main():
    # Whatever the code you want to profile
    pass


if __name__ == "__main__":
    from cProfile import Profile
    from pstats import Stats
    profiler = Profile()
    profiler.enable()
    main()
    # You can also use cProfile.run("main()"), suggested to simple code
    profiler.disable()
    stats = Stats(profiler).sort_stats("cumtime")
    stats.print_stats()
