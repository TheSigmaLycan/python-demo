# generator_demo.py
# Author: Rudhraa

import os
import random
import time

import psutil

os.system("cls")

process_id = psutil.Process(os.getpid())

names = [
    "Rudhraa",
    "Barath",
    "Sarvika",
    "Aswak",
    "Divya",
    "Rakesh",
    "Suresh",
    "Elakkia",
]
langs = [
    "C",
    "C++",
    "Python",
    "Ruby",
    "Kotlin",
    "R",
    "Java",
    "FORTRAN",
    "COBOL",
    "BASIC",
]


def list_example(n):
    result = []
    for i in range(n):
        person = {
            "id": i,
            "name": random.choice(names),
            "lang": random.choice(langs),
        }
        result.append(person)
    return result


def generator_example(n):
    for i in range(n):
        person = {
            "id": i,
            "name": random.choice(names),
            "lang": random.choice(langs),
        }
        yield person


# Get the RAM usage before executing the function

t1 = time.perf_counter()  # Get the time before executing the function
m0 = psutil.Process.memory_info(process_id)

people = list_example(1_000_000)

# people = generator_example(1_000_000)

t2 = time.perf_counter()  # Get the time before executing the function
m1 = psutil.Process.memory_info(process_id)

print(f"Time to execute:\t\t{t2 - t1} s")
print(f"Memory consumption before:\t{m0.rss}\t kiloBytes")  # Resident Set Size
print(f"Memory consumption after:\t{m1.rss}\t kiloBytes")
