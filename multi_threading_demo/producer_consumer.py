import logging
from concurrent.futures import ThreadPoolExecutor
from queue import Queue
from random import randint
from threading import Event
from time import sleep


def producer(queue: Queue, event: Event):
    """ Pretend we're getting a number from the network. """
    while not event.is_set():
        message = randint(1, 101)
        logging.info(f"Producer giving message:\t{message}")
        queue.put(message)

    logging.info("Producer received event. Exiting")


def consumer(queue: Queue, event: Event):
    """ Pretend we're saving a number in the database. """
    while not event.is_set() or not queue.empty():
        message = queue.get()
        logging.info(f"Consumer getting message:\t{message} (size={queue.qsize()})")

    logging.info("Consumer received event. Exiting")


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    pipeline = Queue(maxsize=10)
    event = Event()
    with ThreadPoolExecutor(max_workers=2) as executor:
        executor.submit(producer, pipeline, event)
        executor.submit(consumer, pipeline, event)

        sleep(0.1)
        logging.info("Main: about to set event")
        event.set()
