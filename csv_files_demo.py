# csv_files_demo.py
# Author: Rudhraa

# CSV Comma Separated values
# it is a data file just like Spreadsheets

import pandas as pd

a = {
    "Team 1": ["Rudhraa", "Aswak", "Sarvika", "Barath"],
    "Team 2": ["Aswak", "Elakkia", "Rakesh", "Naren"],
    "Team 3": ["Elakhya", "Someone", None, None],
    "Team 4": ["Sarvika", "Someone", None, None]
}

df = pd.DataFrame(a)

df.to_csv("file.csv", index=False, header=True, encoding="utf-8")
