######################################################################
# Note:
# unittests can only be useful when you're testing deterministic codes
# There are many testing frameworks for python e.g. PyTest, nosetest
# I just think Unittest is easy and enough for almost all the cases.
######################################################################

import unittest

from unittest_demo.arithmetic import addition, subtraction


NUMERO_UNO = 3.0
NUMERO_DOS = 2.0


class MyUnitTests(unittest.TestCase):
    def test_myfunction(self):
        self.assertTrue(True)

    def test_subtraction(self):
        value = subtraction(3, 2)
        self.assertEqual(value, 1)
        value = subtraction(2, 3)
        self.assertEqual(value, -1)
        value = subtraction(-3, -2)
        self.assertEqual()

    def test_multiplication(self):
        value = NUMERO_UNO * NUMERO_DOS
        assert value == 6.0

    def test_addition(self):
        value = addition(1, 2)
        self.assertEqual(value, 3)


if __name__ == "__main__":
    unittest.main()
