# client.py
# Author: Rudhraa S

from socket import *

HOST = "127.0.0.1"
PORT = 1234


def my_client(host, port):
    sock = socket(AF_INET, SOCK_STREAM)
    sock.connect((host, port))
    while True:
        try:
            n = input()
            sock.sendall(str(n).encode("ascii"))
            data = sock.recv(1024)
            print(data.decode("ascii"))
        except KeyboardInterrupt:
            break


if __name__ == "__main__":
    my_client(HOST, PORT)
