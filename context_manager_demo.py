# context_manager_demo.py
# Author: Rudhraa

from time import time

# Context managers are useful in memory management
# by deleting the objects from memory after its use

# For this specific example, you should know file handling first
# But context managers can work with any object

# example without context manager

my_file = open("my_text_file.txt", "w")
my_file.writelines(
    ["This is a newly created file\n", f"This line is written at {time()}"]
)
my_file.close()  # here we have to close the file object explicitly

# with context manager

with open("my_text_file.txt", "r") as my_file:
    text = (my_file.readlines())
    # the context manager closes the file when the block ends

print(text)
