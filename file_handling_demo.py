# file_handling_demo.py
# Author: Rudhraa

# modes can be r, w, a, r+, w+
# to write a file (if the file doesn't exist, it'll be created)

my_file = open("my_text_file.txt", "w")  # open the file
my_file.writelines("This is a newly created file")  # write whatever
my_file.close()  # don't forget to close the file


# to read a file (if the file doesn't exist, it'll throw an error)

my_file = open("my_text_file.txt", "r")  # open the file
text = my_file.readlines()  # read whatever's in the file
my_file.close()  # also don't forget to close the file

print(text)
