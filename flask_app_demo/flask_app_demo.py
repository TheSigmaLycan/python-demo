from flask import Flask, request
from flask.templating import render_template

app = Flask(__name__)

cred = {"srudhraa@gmail.com": "1234asdfasdf"}


@app.route("/")
def hello():
    return render_template("home.html")


@app.route("/home")
def home():
    return "my_home_page"


@app.route("/loginstatus", methods=["GET", "POST"])
def login():
    username = request.form["emailid"]  # get by name
    password = request.form["passwrd"]  # get by name
    if username in cred and password==cred[username]:
        return "Successful"
    else:
        return "Login Failed"


if __name__ == "__main__":
    app.run()
