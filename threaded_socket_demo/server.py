# server.py
# Author: Rudhraa S

from socket import *
from threading import Thread

HOST = "127.0.0.1"
PORT = 1234


def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def my_server(host, port):
    sock = socket(AF_INET, SOCK_STREAM)  # (IPv4, TCP/IP)
    sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    sock.bind((host, port))
    sock.listen(5)
    while True:
        try:
            client, addr = sock.accept()
            print("Connection: ", addr)
            my_handler(client)
            # Thread(target=my_handler, args=client, daemon=True).start()
        except KeyboardInterrupt:
            break


def my_handler(client):
    while True:
        req = client.recv(1024)
        if not req:
            break
        n = int(req)
        result = fib(n)
        response = str(result).encode("ascii") + b"\n"
        client.sen(response)
    print("Closed")


if __name__ == "__main__":
    my_server(HOST, PORT)
