# pyinstrument_demo.py
# Author: Rudhraa


def main():
    # Whatever the code you want to profile
    pass


if __name__ == "__main__":
    from pyinstrument import Profiler
    profiler = Profiler()
    profiler.start()
    main()
    profiler.stop()
    profiler.open_in_browser()
