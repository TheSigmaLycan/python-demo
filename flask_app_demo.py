from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello():
    with open("home.html", "r") as myfile:
        my_home_page = myfile.read()
    return my_home_page


@app.route("/home")
def home():
    return "my_home_page"


if __name__ == "__main__":
    app.run(debug=True)
