# type_demo.py
# Author: Rudhraa

"""Just to let people know int, str, float, etc. are
not really datatypes in python they are in built classes
and associated attributes with which objects are created.
for example:

    >>> a = 20

    The above statement actually means 'a' is an object
    which has a value of '20' the relevant type of object
    is determined by python automatically

    In this case the class used to create object 'a' is 'int'
    You can verify this as shown below

    >>> type(a)
    <class 'int'>

"""

import os

os.system("cls")

from other_python_folder.other_python_file import TheOtherClass


my_str = "Yeeeee Haaaww"

some_number = 50

pi = 3.14

c_num = 3j + 10

my_list = ["A", "B", "C", "D"]

my_tuple = ("A", "B", "C", "D")

my_dict = {"A": "a", "B": "b", "C": "c", "D": "d"}

my_set = {"A", "B", "C", "D"}

a = 10

a = str(a)


def example_function():
    pass


class ExampleClass:
    def __add__():
        pass

    pass


example_object = ExampleClass()

another_example_object = TheOtherClass(1, 2, 3)


print("The type is : ", type(my_str))
print("The type is : ", type(some_number))
print("The type is : ", type(pi))
print("The type is : ", type(c_num))
print("The type is : ", type(my_list))
print("The type is : ", type(my_tuple))
print("The type is : ", type(my_dict))
print("The type is : ", type(my_set))
print("The type is : ", type(ExampleClass))
print("The type is : ", type(example_function))
print("The type is : ", type(example_object))
print("The type is : ", type(a))
print("The type is : ", type(another_example_object))
